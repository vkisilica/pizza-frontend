let routerServiceInstance = null;

const SIGN_IN = 'login';
const SIGN_UP = 'sign-up';
const PROFILE = 'profile';
const CART = 'cart';
const ORDER = 'order';
const ORDER_HISTORY = 'order-history'

const SEPARATOR = '/';

export default class RouterService {
    static factory() {
        if (routerServiceInstance === null) {
            routerServiceInstance = new RouterService();
        }

        return routerServiceInstance;
    }

    getSignInRoute = () => {
        return SEPARATOR + SIGN_IN;
    };

    getSignUpRoute = () => {
        return SEPARATOR + SIGN_UP;
    };

    getProfileRoute = () => {
        return SEPARATOR + PROFILE;
    };

    getCartRoute = () => {
        return SEPARATOR + CART;
    };

    getOrderRoute = () => {
        return SEPARATOR + ORDER;
    };

    getOrderHistoryRoute = () => {
        return SEPARATOR + ORDER_HISTORY;
    };

    getHomeRoute = () => {
        return SEPARATOR;
    };
}

export const appRouter = RouterService.factory();