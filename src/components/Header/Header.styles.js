export default (theme) => ({
    right: {
        marginLeft: 'auto',
        display: 'flex'
    },
    cart: {
        color: '#ffffff',
        '& > a': {
            color: "inherit"
        }
    },
    header: {
        zIndex: 10000
    },
    popper: {
        zIndex: '10000 !important'
    },
    link: {
        textDecoration: 'none',
        color: theme.palette.text.primary,
        '&:hover': {
            color: theme.palette.text.primary
        }
    }
});