import React from 'react';
import PropTypes from "prop-types";
import {Link} from "react-router-dom";

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import AccountCircle from '@material-ui/icons/AccountCircleOutlined';
import ShoppingCartOutlined from '@material-ui/icons/ShoppingCartOutlined';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import withStyles from '@material-ui/core/styles/withStyles';

import UserService from "../../service/user-service";

import styles from './Header.styles';
import {appRouter} from "../../service/router-service";
import Currency from "../../containers/Currency";

const userService = UserService.factory();

class Header extends React.PureComponent{
    state = {
        anchorEl: null
    };

    handleMenu = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleClose = () => {
        this.setState({anchorEl: null});
    };

    handleLogout = () => {
        userService.logout();
        this.handleClose();
        this.props.logout();
    };

    render() {
        const {classes, isAuth} = this.props;
        const {anchorEl} = this.state;
        const isOpenAvatarMenu = Boolean(anchorEl);

        return(
            <AppBar position="fixed" className={classes.header}>
                <Toolbar>
                    <Typography variant="h6">
                        <Button
                            component={Link}
                            to={appRouter.getHomeRoute()}
                            color="inherit"
                        >
                            Pizza
                        </Button>
                    </Typography>
                    <div className={classes.right}>
                        <Currency />
                        <div className={classes.cart}>
                            <IconButton
                                component={Link}
                                to={appRouter.getCartRoute()}
                                color="inherit"
                            >
                                <ShoppingCartOutlined/>
                            </IconButton>
                        </div>
                        {isAuth &&
                            <div>
                                <IconButton
                                    aria-haspopup="true"
                                    onClick={this.handleMenu}
                                    color="inherit"
                                >
                                    <AccountCircle/>
                                </IconButton>
                                <Menu
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={isOpenAvatarMenu}
                                    onClose={this.handleClose}
                                    PopoverClasses={{
                                        root: classes.popper
                                    }}
                                >
                                    <MenuItem onClick={this.handleClose}>
                                        <Link to={appRouter.getProfileRoute()}
                                              className={classes.link}
                                        >
                                            Profile
                                        </Link>
                                    </MenuItem>
                                    <MenuItem onClick={this.handleClose}>
                                        <Link to={appRouter.getOrderHistoryRoute()}
                                              className={classes.link}
                                        >
                                            Order history
                                        </Link>
                                    </MenuItem>
                                    <MenuItem onClick={this.handleLogout}>
                                        <Link to={appRouter.getSignInRoute()}
                                              className={classes.link}
                                        >
                                            Sign out
                                        </Link>
                                    </MenuItem>
                                </Menu>
                            </div>
                        }

                        {isAuth === false &&
                            <Button
                                component={Link}
                                to={appRouter.getSignInRoute()}
                                color="inherit"
                            >
                                Sign In
                            </Button>
                        }
                        </div>
                </Toolbar>
            </AppBar>
        );
    }
}

Header.propTypes = {
    isAuth: PropTypes.bool,
    logout: PropTypes.func,
};

export default withStyles(styles)(Header);