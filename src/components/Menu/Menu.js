import React from 'react';
import {Link} from "react-router-dom";

import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HomeIcon from '@material-ui/icons/HomeOutlined';
import withStyles from '@material-ui/core/styles/withStyles';

import {appRouter} from "../../service/router-service";

import styles from './Menu.styles';

class Menu extends React.PureComponent{
    render() {
        const {classes, isOpen} = this.props;

        return(
            <Drawer
                variant="persistent"
                anchor="left"
                open={isOpen}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <List>
                    <ListItem button className={classes.listItem}>
                        <Link to={appRouter.getHomeRoute()} className={classes.link}>
                            <ListItemIcon><HomeIcon /></ListItemIcon>
                            <ListItemText primary={'Home'} />
                        </Link>
                    </ListItem>
                </List>
            </Drawer>
        );
    }
}

export default withStyles(styles)(Menu);