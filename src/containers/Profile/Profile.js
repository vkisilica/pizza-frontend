import React from 'react';
import PropTypes from "prop-types";
import get from 'lodash/get';
import {Redirect} from "react-router";

import withStyles from '@material-ui/core/styles/withStyles';

import ProfileInfoContainer from './ProfileInfoContainer';

import * as Enum from "./enum";

import {appRouter} from "../../service/router-service";
import UserService from "../../service/user-service";

import connect from './Profile.connect';
import styles from './Profile.styles';

const userService = UserService.factory();

class Profile extends React.PureComponent{

    componentDidMount() {
        this.getUserData();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (Object.keys(prevProps.user).length === 0 && Object.keys(this.props.user).length > 0) {
            const {user} = this.props;

            this.props.actions.changeProfileField({destination: Enum.EMAIL_FIELD, value: get(user, ['attributes', Enum.EMAIL_FIELD], '')});
            this.props.actions.changeProfileField({destination: Enum.NAME_FIELD, value: get(user, ['attributes', Enum.NAME_FIELD], '')});
        }
    }

    getUserData = () => {
        if (Object.keys(this.props.user).length === 0){
            this.props.actions.getUserData();
        }
    };

    render() {
        const {classes, auth} = this.props;
        const isAuth = userService.isAuth() && auth;

        if (!isAuth) return <Redirect to={appRouter.getSignInRoute()} />;

        return(
            <div className={classes.root}>
                <div className={classes.form}>
                    <ProfileInfoContainer />
                </div>
            </div>
        );
    }
}

Profile.propTypes = {
    classes: PropTypes.object,
    actions: PropTypes.object,
    disableButton: PropTypes.bool,
    auth: PropTypes.bool,
};

export default connect(withStyles(styles)(Profile));