import {createLogic} from "redux-logic";
import get from 'lodash/get';

import * as C from './constants';
import actions from '../../layout/actions';

import Service from './service';

import * as Enum from "./enum";
import {getFormDataForChangeInfo} from "./getters";

const service = new Service();

const changeProfileInfo = createLogic({
    type: C.CHANGE_PROFILE_INFO,
    latest: true,
    process({getState, action}, dispatch, done) {
        const state = getState();
        const formData = getFormDataForChangeInfo(state);

        dispatch(actions.fetchingTrue({destination: Enum.CHANGE_PROFILE_FETCHING}));

        service.changeProfileInfo(formData)
            .then((res) => {
                dispatch(actions.setUserData(res.data));
                dispatch(actions.fetchingSuccess(['Data has been changed successfully']));
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.CHANGE_PROFILE_FETCHING}));
                return done();
            });
    }
});


export default [
    changeProfileInfo
];
