import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import actions from "../actions";
import * as Enum from '../enum';
import {getFieldValue} from '../getters';
import {getUser} from '../../../layout/getters';

const mapStateToProps = (state) => {
    return {
        user: getUser(state),
        email: getFieldValue(state, Enum.EMAIL_FIELD),
        name: getFieldValue(state, Enum.NAME_FIELD),
        disableButton: getFieldValue(state, Enum.NAME_FIELD).length === 0
        ,
    };
};

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(actions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps);
