import React from 'react';
import PropTypes from "prop-types";
import get from "lodash/get";

import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/Button";
import withStyles from '@material-ui/core/styles/withStyles';

import * as Enum from '../enum';

import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";

import connect from './ProfileInfoContainer.connect';
import styles from './ProfileInfoContainer.styles';

class ProfileInfoContainer extends React.PureComponent{
    state = {
        editMode: false,
    };

    editButtonClickHandler = () => {
        if (this.state.editMode){
            this.setEditModeFalse();
        } else {
            this.setEditModeTrue();
        }
    };

    setEditModeTrue = () => {
        this.setState({editMode: true});
    };

    setEditModeFalse = () => {
        this.setState({editMode: false});
    };

    changeField = (destination) => (e) => {
        this.props.actions.changeProfileField({destination, value: get(e, 'target.value', '')})
    };

    changeClickButtonHandler = () => {
        this.setEditModeFalse();
        this.props.actions.changeProfileInfo();
    };

    render() {
        const {classes, email, name, user, disableButton} = this.props;
        const {editMode} = this.state;

        return(
            <>
                <IconButton onClick={this.editButtonClickHandler}
                            color={editMode ? 'primary' : ''}
                            className={classes.iconButton}
                >
                    <EditIcon />
                </IconButton>
                <TextField label="E-mail"
                           className={classes.textField}
                           onChange={this.changeField(Enum.EMAIL_FIELD)}
                           value={email !== '' ? email : user.email}
                           disabled
                />
                <TextField label="Name"
                           className={classes.textField}
                           onChange={this.changeField(Enum.NAME_FIELD)}
                           value={name !== '' ? name : user.name}
                           disabled={!editMode}
                />

                {editMode &&
                    <Button color="primary"
                            variant="contained"
                            className={classes.button}
                            disabled={disableButton}
                            onClick={this.changeClickButtonHandler}
                    >
                        Update
                    </Button>
                }
            </>
        );
    }
}

ProfileInfoContainer.propTypes = {
    classes: PropTypes.object,
    actions: PropTypes.object,
    disableButton: PropTypes.bool
};

export default connect(withStyles(styles)(ProfileInfoContainer));