import BaseService from "../../service/base-service";

class ProfileService extends BaseService{
    changeProfileInfo(formData) {
        return this.put('auth/user', formData);
    }
}

export default ProfileService;