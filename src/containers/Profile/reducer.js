import createReducer from "../../store/createReducer";
import * as C from './constants';
import * as Enum from './enum';

export const GENERAL_PATH = 'profile';

export const initialState = {
    [Enum.EMAIL_FIELD]: '',
    [Enum.NAME_FIELD]: '',
};

const changeField = (state, {payload}) => ({
    ...state,
    [payload.destination]: payload.value
});

export const reducer = createReducer(initialState, {
    [C.CHANGE_PROFILE_FIELD]: changeField,
});