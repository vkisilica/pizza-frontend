import React from 'react';
import {Link} from "react-router-dom";
import {Redirect} from "react-router";
import PropTypes from "prop-types";
import get from "lodash/get";

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography  from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

import {appRouter} from '../../service/router-service';
import UserService from "../../service/user-service";

import * as Enum from './enum';

import connect from './SignUp.connect';
import styles from './SignUp.styles';

const userService = UserService.factory();

class SignUp extends React.PureComponent{
    state = {
        passwordFieldIsFocused: false
    };

    componentWillUnmount() {
        this.props.actions.signUpPageDown();
    }

    changeField = (destination) => (e) => {
        this.props.actions.signUpChangeField({destination, value: get(e, 'target.value', '')})
    };

    clickButtonHandler = () => {
        this.props.actions.signUp();
    };

    passwordFieldFocus = () => {
        this.setState({passwordFieldIsFocused: true});
    };

    passwordFieldBlur = () => {
        this.setState({passwordFieldIsFocused: false});
    };

    render() {
        const {classes, disableButton, isPasswordError, auth,
            username, name, password, passwordRepeat } = this.props;
        const {passwordFieldIsFocused} = this.state;
        const showPasswordError = isPasswordError && !passwordFieldIsFocused;

        const isAuth = userService.isAuth() && auth;

        if (isAuth) return <Redirect to={appRouter.getHomeRoute()} />;

        return(
            <div className={classes.root}>
                <div className={classes.form}>
                    <TextField label="Username"
                               className={classes.textField}
                               onChange={this.changeField(Enum.USERNAME_FIELD)}
                               value={username}
                    />
                    <TextField label="Name"
                               className={classes.textField}
                               onChange={this.changeField(Enum.NAME_FIELD)}
                               value={name}
                    />
                    <TextField label="Password"
                               className={classes.textField}
                               onChange={this.changeField(Enum.PASSWORD_FIELD)}
                               type="password"
                               value={password}
                    />
                    <TextField label="Confirm password"
                               className={classes.textField}
                               onChange={this.changeField(Enum.PASSWORD_REPEAT_FIELD)}
                               type="password"
                               error={showPasswordError}
                               onFocus={this.passwordFieldFocus}
                               onBlur={this.passwordFieldBlur}
                               helperText={showPasswordError && 'Passwords are not equal'}
                               value={passwordRepeat}
                    />

                    <Button color="primary"
                            variant="contained"
                            className={classes.button}
                            disabled={disableButton}
                            onClick={this.clickButtonHandler}
                    >
                        Sign up
                    </Button>

                    <Typography className={classes.noAccount}>
                        Already have an account?&nbsp;
                        <Link to={appRouter.getSignInRoute()}
                              className={classes.link}>
                            Sign in
                        </Link>
                    </Typography>
                </div>
            </div>
        );
    }
}

SignUp.propTypes = {
    username: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    password: PropTypes.string,
    passwordRepeat: PropTypes.string,
    classes: PropTypes.object,
    actions: PropTypes.object,
    disableButton: PropTypes.bool,
    isPasswordError: PropTypes.bool,
    auth: PropTypes.bool,
};

export default connect(withStyles(styles)(SignUp));