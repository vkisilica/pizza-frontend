export const USERNAME_FIELD = 'email';
export const NAME_FIELD = 'name';
export const PASSWORD_FIELD = 'password';
export const PASSWORD_REPEAT_FIELD = 'password_confirmation';

export const SIGN_UP_FETCHING = 'SIGN_UP_FETCHING';