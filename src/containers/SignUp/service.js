import BaseService from "../../service/base-service";

class SignInService extends BaseService{
    signUp(formData) {
        return this.post('/auth/register', formData);
    }
}

export default SignInService;