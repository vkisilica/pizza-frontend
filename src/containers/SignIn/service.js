import BaseService from "../../service/base-service";

class SignInService extends BaseService{
    signIn(password, username){
        const formData = new FormData();

        formData.append('password', password);
        formData.append('email', username);

        return this.post('auth/login', formData);
    }
}

export default SignInService;