import React from 'react';
import PropTypes from "prop-types";
import {Redirect} from "react-router";

import Typography from "@material-ui/core/Typography";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import withStyles from '@material-ui/core/styles/withStyles';

import {appRouter} from "../../service/router-service";
import UserService from "../../service/user-service";

import connect from './OrderHistory.connect';
import styles from './OrderHistory.styles';
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {map} from "lodash/collection";
import get from "lodash/get";

const userService = UserService.factory();

class OrderHistory extends React.PureComponent{
    componentDidMount() {
        this.props.actions.getOrderHistory();
    }

    render() {
        const {classes, auth, orders} = this.props;
        const isAuth = userService.isAuth() && auth;

        if (!isAuth) return <Redirect to={appRouter.getSignInRoute()} />;

        return(
            <div className={classes.root}>
                <Typography variant="h2">
                    Order history
                </Typography>
                <div className={classes.orders}>
                    {orders.map(order => {
                        return (
                            <ExpansionPanel className={classes.order} id={`panel${order.id}`} key={`panel${order.id}`}>
                                <ExpansionPanelSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id={`panel${order.id}-header`}
                                    key={`panel${order.id}-header`}
                                >
                                    <Typography className={classes.heading}>Order #{get(order, 'id', '')}</Typography>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails className={classes.expended}>
                                    <div className={classes.cart}>
                                        <TableContainer component={Paper}>
                                            <Table className={classes.table} aria-label="simple table">
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell>Title</TableCell>
                                                        <TableCell align="center">Amount</TableCell>
                                                        <TableCell align="center">Cost</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {map(order.products, (product) => (
                                                        <TableRow key={product.id}>
                                                            <TableCell component="th" scope="row">
                                                                <Typography variant="body1">
                                                                    {get(product, 'product.title', '')}
                                                                </Typography>
                                                            </TableCell>
                                                            <TableCell align="center">
                                                                <Typography variant="body1">
                                                                    {get(product, 'amount', 0)}
                                                                </Typography>
                                                            </TableCell>
                                                            <TableCell align="center">
                                                                <Typography variant="body1">
                                                                    {get(product, 'cost', '')}&nbsp;
                                                                    {get(order, 'currency', '')}
                                                                </Typography>
                                                            </TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </div>
                                    <div className={classes.orderDetails}>
                                        <div className={classes.contacts}>
                                            <Typography variant={"body1"}>
                                                <strong>E-mail</strong>: {get(order, 'contact.email', '')}
                                            </Typography>
                                            <Typography variant={"body1"}>
                                                <strong>Phone</strong>: {get(order, 'contact.phone', '')}
                                            </Typography>
                                        </div>
                                        <div className={classes.address}>
                                            <Typography variant={"body1"}>
                                                <strong>Street</strong>: {get(order, 'address.street.title', '')}
                                            </Typography>
                                            <Typography variant={"body1"}>
                                                <strong>House</strong>: {get(order, 'address.house', '')}
                                            </Typography>
                                            <Typography variant={"body1"}>
                                                {get(order, 'address.apartment', '') ? <><strong>Apartment</strong>: {get(order, 'address.apartment', '')}</> : ""}
                                            </Typography>
                                        </div>
                                        <div className={classes.summary}>
                                            <Typography variant={"body1"}>
                                                <strong>Cart cost</strong>: {get(order, 'cart_cost', '')} {get(order, 'currency', '')}
                                            </Typography>
                                            <Typography variant={"body1"}>
                                                <strong>Delivery cost</strong>: {get(order, 'delivery_cost', '')} {get(order, 'currency', '')}
                                            </Typography>
                                            <Typography variant={"body1"}>
                                                <strong>Total cost</strong>: {get(order, 'total_cost', '')} {get(order, 'currency', '')}
                                            </Typography>
                                        </div>
                                    </div>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        );
                    })}
                </div>
            </div>
        );
    }
}

OrderHistory.propTypes = {
    classes: PropTypes.object,
    actions: PropTypes.object,
    orders: PropTypes.array,
    auth: PropTypes.bool,
};

export default withStyles(styles)(connect(OrderHistory));