export default (theme) => ({
    root: {
        // display: 'flex',
        // flexWrap: 'wrap',
    },
    orders: {

    },
    order: {
        border: '1px solid',
        margin: '10px auto'
    },
    expended: {
        display: 'flex',
        flexDirection: 'column'
    },
    orderDetails: {
        display: 'flex',
    },
    cart: {
        width: '100%',
        marginBottom: '10px'
    },
    contacts: {
        display: 'flex',
        flexDirection: 'column',
        width: '15%'
    },
    address: {
        display: 'flex',
        flexDirection: 'column',
        width: '15%'
    },
    summary: {
        display: 'flex',
        flexDirection: 'column',
        width: '15%'
    },
});