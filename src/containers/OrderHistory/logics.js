import {createLogic} from "redux-logic";
import get from 'lodash/get';

import * as C from './constants';
import actions from '../../layout/actions';
import orderActions from './actions';

import Service from './service';
import * as Enum from "./enum";

const service = new Service();

const getOrderHistory = createLogic({
    type: C.GET_ORDER_HISTORY,
    latest: true,
    process({getState, action}, dispatch, done) {
        dispatch(actions.fetchingTrue({destination: Enum.GET_ORDER_HISTORY_FETCHING}));

        service.getHistory()
            .then((res) => {
                dispatch(actions.fetchingSuccess());
                dispatch(orderActions.setOrderHistory(res.data));
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.GET_ORDER_HISTORY_FETCHING}));
                return done();
            });
    }
});

export default [
    getOrderHistory,
];
