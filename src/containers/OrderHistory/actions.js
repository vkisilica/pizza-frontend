import createAction from "../../store/createAction";

import * as C from './constants';

const getOrderHistory = createAction(C.GET_ORDER_HISTORY, 'payload');
const setOrderHistory = createAction(C.SET_ORDER_HISTORY, 'payload');

export default {
    getOrderHistory,
    setOrderHistory,
}