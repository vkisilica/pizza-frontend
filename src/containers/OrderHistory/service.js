import BaseService from "../../service/base-service";

class OrderHistory extends BaseService {
    getHistory(){
        return this.get('orders/history')
    }
}

export default OrderHistory;