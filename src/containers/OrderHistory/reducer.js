import createReducer from "../../store/createReducer";
import * as C from './constants';
import * as Enum from './enum';

export const GENERAL_PATH = 'order_history';

export const initialState = {
    [Enum.ORDERS]: [],
};

const setOrderHistory = (state, {payload}) => ({
    ...state,
    [Enum.ORDERS]: payload,
});

export const reducer = createReducer(initialState, {
    [C.SET_ORDER_HISTORY]: setOrderHistory,
});