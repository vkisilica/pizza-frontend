import createReducer from "../../store/createReducer";
import * as C from './constants';
import * as Enum from './enum';

export const GENERAL_PATH = 'currency';

export const initialState = {
    [Enum.CURRENT_CURRENCY]: "",
};

const setCurrentCurrency = (state, {payload}) => ({
    ...state,
    [Enum.CURRENT_CURRENCY]: payload,
});

export const reducer = createReducer(initialState, {
    [C.SET_CURRENT_CURRENCY]: setCurrentCurrency,
});