import BaseService from "../../service/base-service";

class CurrencyService extends BaseService{
    getCurrentCurrency(){
        return this.get('auth/currency')
    }
    changeCurrentCurrency(formData){
        return this.put('auth/currency', formData)
    }
}

export default CurrencyService;