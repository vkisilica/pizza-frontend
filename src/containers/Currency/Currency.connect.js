import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import actions from "./actions";
import {getCurrentCurrency} from './getters';

const mapStateToProps = (state) => {
    return {
        currentCurrency: getCurrentCurrency(state),
    };
};

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(actions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps);
