import get from 'lodash/get';
import {GENERAL_PATH} from "./reducer";
import * as Enum from './enum';

const getStateData = (state) => get(state, GENERAL_PATH, {});

export const getFormDataForChangeCurrentCurrency = (state) => {
    const formData = new FormData();

    formData.append(Enum.CURRENT_CURRENCY, getCurrentCurrency(state));

    return formData;
};

export const getCurrentCurrency = (state) => get(getStateData(state), Enum.CURRENT_CURRENCY, "USD");