import {createLogic} from "redux-logic";
import get from 'lodash/get';

import * as C from './constants';
import actions from '../../layout/actions';
import currencyActions from './actions';

import Service from './service';
import * as Enum from "./enum";

const service = new Service();

const getCurrentCurrency = createLogic({
    type: C.GET_CURRENT_CURRENCY,
    latest: true,
    process({getState, action}, dispatch, done) {
        dispatch(actions.fetchingTrue({destination: Enum.GET_CURRENT_CURRENCY_FETCHING}));

        service.getCurrentCurrency()
            .then((res) => {
                dispatch(actions.fetchingSuccess());
                dispatch(currencyActions.setCurrentCurrency(res.data.currency));
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.GET_CURRENT_CURRENCY_FETCHING}));
                return done();
            });
    }
});

const changeCurrentCurrency = createLogic({
    type: C.CHANGE_CURRENT_CURRENCY,
    latest: true,
    process({getState, action}, dispatch, done) {
        dispatch(actions.fetchingTrue({destination: Enum.CHANGE_CURRENT_CURRENCY_FETCHING}));

        const formData = new FormData();
        formData.append(Enum.CURRENT_CURRENCY, action.payload);
        service.changeCurrentCurrency(formData)
            .then((res) => {
                dispatch(actions.fetchingSuccess());
                dispatch(currencyActions.setCurrentCurrency(res.data.currency));
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.CHANGE_CURRENT_CURRENCY_FETCHING}));
                return done();
            });
    }
});

export default [
    getCurrentCurrency,
    changeCurrentCurrency
];
