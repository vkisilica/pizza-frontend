import createAction from "../../store/createAction";

import * as C from './constants';

const getCurrentCurrency = createAction(C.GET_CURRENT_CURRENCY, 'payload');
const setCurrentCurrency = createAction(C.SET_CURRENT_CURRENCY, 'payload');

const changeCurrentCurrency = createAction(C.CHANGE_CURRENT_CURRENCY, 'payload');

export default {
    getCurrentCurrency,
    setCurrentCurrency,

    changeCurrentCurrency,
}