import React from 'react';
import PropTypes from "prop-types";

import Button from '@material-ui/core/Button';
import withStyles from '@material-ui/core/styles/withStyles';

import connect from './Currency.connect';
import styles from './Currency.styles';

class Currency extends React.PureComponent{
    componentDidMount() {
        this.props.actions.getCurrentCurrency();
    }

    clickUSDButtonHandler = () => {
        this.props.actions.changeCurrentCurrency("USD");
    };

    clickEURButtonHandler = () => {
        this.props.actions.changeCurrentCurrency("EUR");
    };

    render() {
        const {classes, currentCurrency} = this.props;

        return(
            <div className={classes.root}>
                <Button color="primary"
                        variant="contained"
                        className={classes.button}
                        onClick={this.clickUSDButtonHandler}
                        disabled={currentCurrency === 'USD'}
                >
                    $
                </Button>

                |

                <Button color="primary"
                        variant="contained"
                        className={classes.button}
                        onClick={this.clickEURButtonHandler}
                        disabled={currentCurrency === 'EUR'}
                >
                    &euro;
                </Button>
            </div>
        );
    }
}

Currency.propTypes = {
    classes: PropTypes.object,
    actions: PropTypes.object,
    activeCurrency: PropTypes.string
};

export default withStyles(styles)(connect(Currency));