export const CURRENT_CURRENCY = 'currency';

export const GET_CURRENT_CURRENCY_FETCHING = 'GET_CURRENT_CURRENCY_FETCHING';
export const CHANGE_CURRENT_CURRENCY_FETCHING = 'CHANGE_CURRENT_CURRENCY_FETCHING';