import React from 'react';
import PropTypes from "prop-types";
import get from 'lodash/get';

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import connect from './Home.connect';
import styles from './Home.styles';

class Home extends React.PureComponent{
    componentDidMount() {
        if (this.props.products.length === 0) {
            this.props.actions.getProducts();
        }
    }

    addToCart = (product) => () => {
        this.props.actions.addProduct(product);
    };

    render() {
        const {classes, products} = this.props;

        return(
            <div className={classes.root}>
                {products.map(product => {

                    return (
                        <Card className={classes.card} key={`product-${product.id}`}>
                            <CardContent>
                                <Typography variant="h5" component="h2">
                                    {get(product, 'title', '')}
                                </Typography>
                                <Typography className={classes.description} color="textSecondary">
                                    {get(product, 'description', '')}
                                </Typography>
                                <hr/>
                                <Typography className={classes.price} color="textSecondary">
                                    {get(product, 'price', 0)} {get(product, 'currency')}
                                </Typography>
                            </CardContent>
                            <CardActions className={classes.actions}>
                                <Button color="primary"
                                        variant="outlined"
                                        onClick={this.addToCart(product)}
                                >
                                    Add to cart
                                </Button>
                            </CardActions>
                        </Card>
                    );
                })}
            </div>
        );
    }
}

Home.propTypes = {
    classes: PropTypes.object,
    actions: PropTypes.object,
    products: PropTypes.array,
    auth: PropTypes.bool,
};

export default withStyles(styles)(connect(Home));