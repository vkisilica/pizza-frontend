import createReducer from "../../store/createReducer";
import * as C from './constants';
import * as Enum from './enum';

export const GENERAL_PATH = 'home';

export const initialState = {
    [Enum.PRODUCTS]: [],
};

const setProducts = (state, {payload}) => ({
    ...state,
    [Enum.PRODUCTS]: payload,
});

export const reducer = createReducer(initialState, {
    [C.SET_PRODUCTS]: setProducts,
});