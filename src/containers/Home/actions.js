import createAction from "../../store/createAction";

import * as C from './constants';

const getProducts = createAction(C.GET_PRODUCTS, 'payload');
const setProducts = createAction(C.SET_PRODUCTS, 'payload');

export default {
    getProducts,
    setProducts,
}