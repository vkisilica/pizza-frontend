import {createLogic} from "redux-logic";
import get from 'lodash/get';

import * as C from './constants';
import * as CurrencyC from '../Currency/constants';
import actions from '../../layout/actions';
import homeActions from './actions';

import Service from './service';
import * as Enum from "./enum";

const service = new Service();

const getProducts = createLogic({
    type: [
        C.GET_PRODUCTS,
        CurrencyC.SET_CURRENT_CURRENCY
    ],
    latest: true,
    process({getState, action}, dispatch, done) {
        dispatch(actions.fetchingTrue({destination: Enum.GET_PRODUCTS_FETCHING}));

        service.getProducts()
            .then((res) => {
                dispatch(actions.fetchingSuccess());
                dispatch(homeActions.setProducts(res.data));
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.GET_PRODUCTS_FETCHING}));
                return done();
            });
    }
});

const addToCart = createLogic({
    type: C.ADD_TO_CART,
    latest: true,
    process({getState, action}, dispatch, done) {
        const productId = action.payload;

        dispatch(actions.fetchingTrue({destination: Enum.ADD_TO_CART_FETCHING}));

        service.addToCart(productId)
            .then((res) => {
                dispatch(actions.fetchingSuccess(['The product has been added']));
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.ADD_TO_CART_FETCHING}));
                return done();
            });
    }
});

export default [
    getProducts,
    addToCart,
];
