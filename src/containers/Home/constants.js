export const GET_PRODUCTS = 'GET_PRODUCTS';
export const SET_PRODUCTS = 'SET_PRODUCTS';

export const ADD_TO_CART = 'ADD_TO_CART';