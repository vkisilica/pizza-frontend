import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import actions from "./actions";
import cartActions from  "../Cart/actions";
import {getProducts} from './getters';

const mapStateToProps = (state) => {
    return {
        products: getProducts(state),
    };
};

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({...actions, ...cartActions}, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps);
