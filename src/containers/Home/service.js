import BaseService from "../../service/base-service";

class HomeService extends BaseService{
    getProducts(){
        return this.get('catalog/products')
    }
}

export default HomeService;