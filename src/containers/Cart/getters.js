import get from 'lodash/get';
import {GENERAL_PATH} from "./reducer";
import * as Enum from './enum';

const getStateData = (state) => get(state, GENERAL_PATH, {});

export const getProducts = (state) => get(getStateData(state), Enum.PRODUCTS, {});
export const getDeliveryCost = (state) => get(getStateData(state), Enum.DELIVERY_COST, 0);
export const getTotalCost = (state) => get(getStateData(state), Enum.TOTAL_COST, 0);