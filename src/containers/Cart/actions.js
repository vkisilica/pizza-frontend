import createAction from "../../store/createAction";

import * as C from './constants';

const getProducts = createAction(C.CART_GET_PRODUCTS, 'payload');
const setProducts = createAction(C.CART_SET_PRODUCTS, 'payload');
const setDeliveryCost = createAction(C.CART_SET_DELIVERY_COST, 'payload');
const setTotalCost = createAction(C.CART_SET_TOTAL_COST, 'payload');

const addProduct = createAction(C.CART_ADD_PRODUCT, 'payload');
const increaseAmountBy = createAction(C.CART_INCREASE_AMOUNT_BY, 'payload');
const decreaseAmountBy = createAction(C.CART_DECREASE_AMOUNT_BY, 'payload');
const removeProduct = createAction(C.CART_REMOVE_PRODUCT, 'payload');

export default {
    getProducts,
    setProducts,
    setDeliveryCost,
    setTotalCost,

    addProduct,
    increaseAmountBy,
    decreaseAmountBy,
    removeProduct,
}