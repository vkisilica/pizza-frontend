import React from 'react';
import PropTypes from "prop-types";
import get from 'lodash/get';
import {map} from 'lodash/collection';

import IconButton from '@material-ui/core/IconButton';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined';
import Typography from '@material-ui/core/Typography';
import Paper from "@material-ui/core/Paper";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import withStyles from '@material-ui/core/styles/withStyles';

import connect from './Cart.connect';
import styles from './Cart.styles';
import Order from "../Order";

class Cart extends React.PureComponent{

    componentDidMount() {
        this.props.actions.getProducts();
    }

    increaseBy = (productId, amount) => () => {
        this.props.actions.increaseAmountBy({productId, amount});
    };

    decreaseBy = (productId, amount) => () => {
        this.props.actions.decreaseAmountBy({productId, amount});
    };

    removeFromCart = (productId) => () => {
        this.props.actions.removeProduct({productId});
    };

    render() {
        const {classes, products, deliveryCost, totalCost, currency} = this.props;

        if (Object.keys(products).length === 0) {
            return(
                <div className={classes.rootEmpty}>
                    <div>
                        <Typography variant="h2">
                            Cart
                        </Typography>
                    </div>
                    <div>
                        <Typography variant="h3">
                            Cart is empty
                        </Typography>
                    </div>
                </div>
            )
        } else {
            return(
                <div className={classes.root}>
                    <div className={classes.cart}>
                        <Typography variant="h2">
                            Cart
                        </Typography>

                        <TableContainer component={Paper}>
                            <Table className={classes.table} aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Title</TableCell>
                                        <TableCell align="center">Amount</TableCell>
                                        <TableCell align="center">Cost</TableCell>
                                        <TableCell align="center">&nbsp;</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {map(products, (product) => (
                                        <TableRow key={product.id}>
                                            <TableCell component="th" scope="row">
                                                <Typography variant="h5">
                                                    {get(product, 'title', '')}
                                                </Typography>
                                            </TableCell>
                                            <TableCell align="center">
                                                <IconButton color="primary"
                                                            variant="outlined"
                                                            onClick={this.decreaseBy(product.id, 1)}
                                                >
                                                    <RemoveCircleOutlineIcon />
                                                </IconButton>

                                                {product.amount}

                                                <IconButton color="primary"
                                                            variant="outlined"
                                                            onClick={this.increaseBy(product.id, 1)}
                                                >
                                                    <AddCircleOutlineIcon />
                                                </IconButton>
                                            </TableCell>
                                            <TableCell align="center">
                                                <Typography variant="h5">
                                                    {get(product, 'cost', '')}&nbsp;
                                                    {currency}
                                                </Typography>
                                            </TableCell>
                                            <TableCell align="center">
                                                <IconButton color="primary"
                                                            variant="outlined"
                                                            onClick={this.removeFromCart(product.id)}
                                                >
                                                    <CloseOutlinedIcon />
                                                </IconButton>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>

                        <div className={classes.total_cost}>
                            <Typography variant="h5">
                                Delivery cost: {deliveryCost} {currency}
                            </Typography>
                            <Typography variant="h5">
                                Total cost: {totalCost} {currency}
                            </Typography>
                        </div>
                    </div>

                    <div className={classes.order}>
                        <Order />
                    </div>
                </div>
            );
        }


    }
}

Cart.propTypes = {
    classes: PropTypes.object,
    actions: PropTypes.object,
    products: PropTypes.object,
    auth: PropTypes.bool,
};

export default withStyles(styles)(connect(Cart));