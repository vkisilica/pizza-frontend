import createReducer from "../../store/createReducer";
import * as C from './constants';
import * as Enum from './enum';

export const GENERAL_PATH = 'cart';

export const initialState = {
    [Enum.PRODUCTS]: {},
    [Enum.DELIVERY_COST]: 0,
    [Enum.TOTAL_COST]: 0,
};

const setProducts = (state, {payload}) => ({
    ...state,
    [Enum.PRODUCTS]: payload.length === 0 ? {} : payload,
});

const setDeliveryCost = (state, {payload}) => ({
    ...state,
    [Enum.DELIVERY_COST]: payload,
});

const setTotalCost = (state, {payload}) => ({
    ...state,
    [Enum.TOTAL_COST]: payload,
});

export const reducer = createReducer(initialState, {
    [C.CART_SET_PRODUCTS]: setProducts,
    [C.CART_SET_TOTAL_COST]: setTotalCost,
    [C.CART_SET_DELIVERY_COST]: setDeliveryCost
});