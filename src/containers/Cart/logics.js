import {createLogic} from "redux-logic";

import * as C from './constants';
import cartActions from './actions';

import Service from './service';
import * as CurrencyC from "../Currency/constants";
import actions from "../../layout/actions";
import * as Enum from "../Home/enum";
import get from "lodash/get";

const service = new Service();

const getProducts = createLogic({
    type: [
        C.CART_GET_PRODUCTS,
        CurrencyC.SET_CURRENT_CURRENCY
    ],
    latest: true,
    process({getState, action}, dispatch, done) {
        dispatch(actions.fetchingTrue({destination: Enum.CART_GET_PRODUCTS_FETCHING}));

        service.getCart()
            .then((res) => {
                // dispatch(actions.fetchingSuccess());
                dispatch(cartActions.setProducts(res.data.products));
                dispatch(cartActions.setDeliveryCost(res.data.delivery_cost));
                dispatch(cartActions.setTotalCost(res.data.total_cost));
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.CART_GET_PRODUCTS_FETCHING}));
                return done();
            });
    }
});

const addProduct = createLogic({
    type: C.CART_ADD_PRODUCT,
    latest: true,
    process({getState, action}, dispatch, done) {
        const product = action.payload;
        dispatch(actions.fetchingTrue({destination: Enum.CART_ADD_PRODUCT_FETCHING}));

        service.addProduct(product)
            .then((res) => {
                dispatch(actions.fetchingSuccess(['The product was added into the cart']));
                dispatch(cartActions.setProducts(res.data.products));
                dispatch(cartActions.setDeliveryCost(res.data.delivery_cost));
                dispatch(cartActions.setTotalCost(res.data.total_cost));
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.CART_ADD_PRODUCT_FETCHING}));
                return done();
            });
    }
});

const increaseAmountBy = createLogic({
    type: C.CART_INCREASE_AMOUNT_BY,
    latest: true,
    process({getState, action}, dispatch, done) {
        const {productId, amount} = action.payload;
        dispatch(actions.fetchingTrue({destination: Enum.CART_ADD_PRODUCT_FETCHING}));

        service.increaseAmountBy(productId, amount)
            .then((res) => {
                dispatch(actions.fetchingSuccess(['Amount of the product in the cart was changed']));
                dispatch(cartActions.setProducts(res.data.products));
                dispatch(cartActions.setDeliveryCost(res.data.delivery_cost));
                dispatch(cartActions.setTotalCost(res.data.total_cost));
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.CART_ADD_PRODUCT_FETCHING}));
                return done();
            });
    }
});

const decreaseAmountBy = createLogic({
    type: C.CART_DECREASE_AMOUNT_BY,
    latest: true,
    process({getState, action}, dispatch, done) {
        const {productId, amount} = action.payload;

        dispatch(actions.fetchingTrue({destination: Enum.CART_REMOVE_PRODUCT_FETCHING}));

        service.decreaseAmountBy(productId, amount)
            .then((res) => {
                dispatch(actions.fetchingSuccess(['Amount of the product in the cart was changed']));
                dispatch(cartActions.setProducts(res.data.products));
                dispatch(cartActions.setDeliveryCost(res.data.delivery_cost));
                dispatch(cartActions.setTotalCost(res.data.total_cost));
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.CART_REMOVE_PRODUCT_FETCHING}));
                return done();
            });
    }
});

const removeProduct = createLogic({
    type: C.CART_REMOVE_PRODUCT,
    latest: true,
    process({getState, action}, dispatch, done) {
        const {productId} = action.payload;

        dispatch(actions.fetchingTrue({destination: Enum.CART_REMOVE_PRODUCT_FETCHING}));

        service.removeProduct(productId)
            .then((res) => {
                dispatch(actions.fetchingSuccess(['The product was removed from the cart']));
                dispatch(cartActions.setProducts(res.data.products));
                dispatch(cartActions.setDeliveryCost(res.data.delivery_cost));
                dispatch(cartActions.setTotalCost(res.data.total_cost));
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.CART_REMOVE_PRODUCT_FETCHING}));
                return done();
            });
    }
});

export default [
    getProducts,
    addProduct,
    increaseAmountBy,
    decreaseAmountBy,
    removeProduct
];
