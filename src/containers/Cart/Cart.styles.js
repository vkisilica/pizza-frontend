export default (theme) => ({
    rootEmpty: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column'
    },
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    actions: {
        padding: 16,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    link: {
        color: theme.palette.primary.main,
        textDecoration: 'none'
    },
    total_cost: {
        marginTop: 10,
        width: '100%'
    },

    cart: {
        width: '70%',
    },

    order: {
        width: '30%',
    }
});