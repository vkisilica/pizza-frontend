import BaseService from "../../service/base-service";

class CartService extends BaseService{
    getCart(){
        return this.get('orders/cart')
    }

    addProduct(product) {
        const formData = new FormData();
        formData.append('amount', 1);

        return this.put('orders/cart/products/' + product.id, formData)
    }

    increaseAmountBy(productId, amount) {
        const formData = new FormData();
        formData.append('amount', amount);

        return this.put('orders/cart/products/' + productId, formData)
    }

    decreaseAmountBy(productId, amount) {
        const formData = new FormData();
        formData.append('amount', parseInt(amount) * -1);

        return this.put('orders/cart/products/' + productId, formData)
    }

    removeProduct(productId) {
        const formData = new FormData();
        return this.delete('orders/cart/products/' + productId, formData)
    }
}

export default CartService