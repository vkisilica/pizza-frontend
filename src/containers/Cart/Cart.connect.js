import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import actions from "./actions";
import {getProducts, getDeliveryCost, getTotalCost} from './getters';
import {getCurrentCurrency} from '../Currency/getters';

const mapStateToProps = (state) => {
    return {
        products: getProducts(state),
        deliveryCost: getDeliveryCost(state),
        totalCost: getTotalCost(state),
        currency: getCurrentCurrency(state),
    };
};

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(actions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps);
