export const FORM = 'form';

export const EMAIL_FIELD = 'email';
export const PHONE_FIELD = 'phone';
export const STREET_FIELD = 'street_id';
export const HOUSE_FIELD = 'house';
export const APARTMENT_FIELD = 'apartment';

export const META = 'meta';
export const STREETS = 'streets';

export const ORDER_FETCHING = 'ORDER_FETCHING';
export const ORDER_ADDRESS_STREETS_FETCHING = 'ORDER_ADDRESS_STREETS_FETCHING';