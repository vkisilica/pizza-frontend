import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import actions from "./actions";
import {getFieldValue, getMetaStreets} from './getters';
import * as Enum from "./enum";
import {getUser} from "../../layout/getters";

const mapStateToProps = (state) => {
    return {
        disableButton: getFieldValue(state, Enum.PHONE_FIELD).length === 0
            || getFieldValue(state, Enum.STREET_FIELD).length === 0
            || getFieldValue(state, Enum.HOUSE_FIELD).length === 0
        ,
        user: getUser(state),
        streets: getMetaStreets(state),
        email: getFieldValue(state, Enum.EMAIL_FIELD),
        phone: getFieldValue(state, Enum.PHONE_FIELD),
        house: getFieldValue(state, Enum.HOUSE_FIELD),
        apartment: getFieldValue(state, Enum.APARTMENT_FIELD),
    };
};

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(actions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps);
