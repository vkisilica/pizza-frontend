import get from 'lodash/get';
import {GENERAL_PATH} from "./reducer";
import * as Enum from './enum';

const getStateData = (state) => get(state, GENERAL_PATH, {});

export const getMeta = (state, field) => get(getStateData(state), Enum.META, {});
export const getMetaStreets = (state, field) => get(getMeta(state), Enum.STREETS, []);

export const getFieldValue = (state, field) => get(getStateData(state), field, '');

export const getFormDataForOrder = (state) => {
    const formData = new FormData();

    formData.append(Enum.EMAIL_FIELD, getFieldValue(state, Enum.EMAIL_FIELD));
    formData.append(Enum.PHONE_FIELD, getFieldValue(state, Enum.PHONE_FIELD));
    formData.append(Enum.STREET_FIELD, getFieldValue(state, Enum.STREET_FIELD));
    formData.append(Enum.HOUSE_FIELD, getFieldValue(state, Enum.HOUSE_FIELD));
    formData.append(Enum.APARTMENT_FIELD, getFieldValue(state, Enum.APARTMENT_FIELD));

    return formData;
};