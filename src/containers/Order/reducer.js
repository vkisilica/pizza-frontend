import createReducer from "../../store/createReducer";
import * as C from './constants';
import * as Enum from './enum';

export const GENERAL_PATH = 'order';

export const initialState = {
    [Enum.FORM]: {
        [Enum.EMAIL_FIELD]: '',
        [Enum.PHONE_FIELD]: '',
        [Enum.STREET_FIELD]: '',
        [Enum.HOUSE_FIELD]: '',
        [Enum.APARTMENT_FIELD]: '',
    },
    [Enum.META]: {
        [Enum.STREETS]: [
            {
                id: 1,
                title: 'Dummy'
            },
            {
                id: 2,
                title: 'Test'
            }
        ]
    }
};

const changeField = (state, {payload}) => ({
    ...state,
    [payload.destination]: payload.value
});

const setStreets = (state, {payload}) => ({
    ...state,
    [Enum.META]: {
        ...state[Enum.META],
        [Enum.STREETS]: payload
    }
});

const pageDown = () => initialState;

export const reducer = createReducer(initialState, {
    [C.ORDER_CHANGE_FIELD]: changeField,
    [C.ORDER_SET_STREETS]: setStreets,
    [C.ORDER_PAGE_DOWN]: pageDown,
    [C.ORDER_CLEAR_ALL_FIELDS]: pageDown,
});