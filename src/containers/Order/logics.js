import {createLogic} from "redux-logic";
import get from 'lodash/get';

import * as C from './constants';
import actions from '../../layout/actions';
import orderPageActions from './actions';
import cartPageActions from '../Cart/actions';
import {getFormDataForOrder} from "./getters";
import * as Enum from "./enum";
import Service from './service';

const service = new Service();

const order = createLogic({
    type: C.ORDER,
    latest: true,
    process({getState, action}, dispatch, done) {
        const state = getState();
        const formData = getFormDataForOrder(state);

        dispatch(actions.fetchingTrue({destination: Enum.ORDER_FETCHING}));

        service.order(formData)
            .then((res) => {
                dispatch(actions.fetchingSuccess(['The order has been created!']));
                dispatch(orderPageActions.orderClearAllFields());
                dispatch(cartPageActions.getProducts());
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.ORDER_FETCHING}));
                return done();
            });
    }
});

const orderAddressStreet = createLogic({
    type: C.ORDER_ADDRESS_STREET,
    latest: true,
    process({getState, action}, dispatch, done) {

        dispatch(actions.fetchingTrue({destination: Enum.ORDER_ADDRESS_STREETS_FETCHING}));

        service.streets()
            .then((res) => {
                dispatch(orderPageActions.orderSetStreets(res.data));
            })
            .catch((err) => {
                dispatch(actions.fetchingFailed({
                    message: get(err, 'message', ''),
                    errors: get(err, 'errors', {})
                }));
            })
            .then(() => {
                dispatch(actions.fetchingFalse({destination: Enum.ORDER_ADDRESS_STREETS_FETCHING}));
                return done();
            });
    }
});

export default [
    order,
    orderAddressStreet,
];
