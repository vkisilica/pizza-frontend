import BaseService from "../../service/base-service";

class OrderService extends BaseService{
    order(formData) {
        return this.post('orders', formData);
    }
    streets() {
        return this.get('orders/addresses/streets');
    }
}

export default OrderService;