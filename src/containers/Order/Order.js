import React from 'react';
import PropTypes from "prop-types";
import get from 'lodash/get';

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import withStyles from '@material-ui/core/styles/withStyles';

import connect from './Order.connect';
import styles from './Order.styles';
import * as Enum from "./enum";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

class Order extends React.PureComponent{

    componentDidMount() {
        this.props.actions.orderNew();

        const {user, email} = this.props;
        if (email === '' && get(user, 'email', '') !== '') {
            this.props.actions.orderChangeField({destination: Enum.EMAIL_FIELD, value: get(user, 'email')})
        }
    }

    componentWillUnmount() {
        this.props.actions.orderPageDown();
    }

    changeField = (destination) => (e) => {
        this.props.actions.orderChangeField({destination, value: get(e, 'target.value', '')})
    };

    clickButtonHandler = () => {
        this.props.actions.order();
    };

    render() {
        const {classes, disableButton, email, phone, streets, house, apartment} = this.props;

        return (
            <div className={classes.root}>
                <div className={classes.form}>
                    <TextField label="E-mail"
                               className={classes.textField}
                               onChange={this.changeField(Enum.EMAIL_FIELD)}
                               value={email}
                    />
                    <TextField label="Phone. Format: (0XXXX) XXXXXX"
                               className={classes.textField}
                               onChange={this.changeField(Enum.PHONE_FIELD)}
                               value={phone}
                    />
                    <FormControl>
                        <InputLabel id="streets-selector-label">Street</InputLabel>
                        <Select
                            labelId="streets-selector-label"
                            defaultValue={""}
                            onChange={this.changeField(Enum.STREET_FIELD)}>
                            {streets.map(street =>
                                <MenuItem value={street.id} key={`street-${street.id}`}>
                                    {get(street, 'title', '')}
                                </MenuItem>
                            )}
                        </Select>
                    </FormControl>
                    <TextField label="House"
                               className={classes.textField}
                               onChange={this.changeField(Enum.HOUSE_FIELD)}
                               value={house}
                    />
                    <TextField label="Apartment"
                               className={classes.textField}
                               onChange={this.changeField(Enum.APARTMENT_FIELD)}
                               value={apartment}
                    />
                    <Button color="primary"
                            variant="contained"
                            className={classes.button}
                            disabled={disableButton}
                            onClick={this.clickButtonHandler}
                    >
                        Order
                    </Button>
                </div>
            </div>
        )
    }
}

Order.propTypes = {
    classes: PropTypes.object,
    actions: PropTypes.object,
    email: PropTypes.string,
    phone: PropTypes.string,
    house: PropTypes.string,
    apartment: PropTypes.string,
};

export default withStyles(styles)(connect(Order));