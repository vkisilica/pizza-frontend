import createAction from "../../store/createAction";

import * as C from './constants';

const order = createAction(C.ORDER, 'payload');
const orderNew = createAction(C.ORDER_ADDRESS_STREET, 'payload');
const orderChangeField = createAction(C.ORDER_CHANGE_FIELD, 'payload');
const orderPageDown = createAction(C.ORDER_PAGE_DOWN, 'payload');
const orderClearAllFields = createAction(C.ORDER_CLEAR_ALL_FIELDS, 'payload');

const orderSetStreets = createAction(C.ORDER_SET_STREETS, 'payload');

export default {
    order,
    orderNew,
    orderChangeField,
    orderPageDown,
    orderClearAllFields,

    orderSetStreets,
}