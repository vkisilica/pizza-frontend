import {combineReducers} from "redux";
import {GENERAL_PATH as signInPath, reducer as signInReducer} from "../containers/SignIn/reducer";
import {GENERAL_PATH as signUpPath, reducer as signUpReducer} from "../containers/SignUp/reducer";
import {GENERAL_PATH as profilePath, reducer as profileReducer} from "../containers/Profile/reducer";
import {GENERAL_PATH as cartPath, reducer as cartReducer} from "../containers/Cart/reducer";
import {GENERAL_PATH as orderPath, reducer as orderReducer} from "../containers/Order/reducer";
import {GENERAL_PATH as orderHistoryPath, reducer as orderHistoryReducer} from "../containers/OrderHistory/reducer";
import {GENERAL_PATH as currencyPath, reducer as currencyReducer} from "../containers/Currency/reducer";
import {GENERAL_PATH as homePath, reducer as homeReducer} from "../containers/Home/reducer";
import {GENERAL_PATH as mainPath, reducer as mainReducer} from "../layout/reducer";

export default combineReducers({
    [mainPath]: mainReducer,
    [signInPath]: signInReducer,
    [signUpPath]: signUpReducer,
    [profilePath]: profileReducer,
    [cartPath]: cartReducer,
    [orderPath]: orderReducer,
    [orderHistoryPath]: orderHistoryReducer,
    [currencyPath]: currencyReducer,
    [homePath]: homeReducer,
});