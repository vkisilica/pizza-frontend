import SignInLogic from '../containers/SignIn/logics';
import SignUpLogic from '../containers/SignUp/logics';
import ProfileLogic from '../containers/Profile/logics';
import HomeLogic from '../containers/Home/logics';
import CartLogic from '../containers/Cart/logics';
import OrderLogic from '../containers/Order/logics';
import OrderHistoryLogic from '../containers/OrderHistory/logics';
import CurrencyLogic from '../containers/Currency/logics';
import MainLogic from '../layout/logics';

export default [
    ...SignInLogic,
    ...SignUpLogic,
    ...ProfileLogic,
    ...MainLogic,
    ...HomeLogic,
    ...CartLogic,
    ...OrderLogic,
    ...OrderHistoryLogic,
    ...CurrencyLogic,
];