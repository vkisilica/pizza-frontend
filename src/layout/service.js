import BaseService from "../service/base-service";

class MainService extends BaseService{
    getUserData(){
        return this.get('/auth/user');
    }
}

export default MainService;