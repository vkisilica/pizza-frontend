export const FETCHING = 'FETCHING';
export const ERRORS = 'ERRORS';
export const ERROR_MESSAGE = 'ERROR_MESSAGE';
export const SUCCESS_MESSAGES = 'SUCCESS_MESSAGES';
export const USER = 'USER';
export const IS_AUTH = 'IS_AUTH';

export const USER_DATA_FETCHING = 'USER_DATA_FETCHING';