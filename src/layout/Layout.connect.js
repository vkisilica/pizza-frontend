import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import actions from "./actions";
import homeActions from "../containers/Home/actions";
import {isFetching, getErrors, getSuccessMessages, getAuth, getUser} from './getters';

const mapStateToProps = (state) => {
    return {
        fetching: isFetching(state),
        errors: getErrors(state),
        successMessages: getSuccessMessages(state),
        auth: getAuth(state),
        user: getUser(state)
    };
};

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators({...homeActions, ...actions}, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps);
