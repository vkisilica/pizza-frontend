import React from "react";
import {BrowserRouter as Router, Route, Redirect, Switch} from 'react-router-dom';

import Layout from './layout';
import SignIn from './containers/SignIn';
import SignUp from './containers/SignUp';
import Profile from './containers/Profile';
import Home from './containers/Home';
import Cart from "./containers/Cart";
import OrderHistory from "./containers/OrderHistory";

import RouterService from './service/router-service';
import UserService from './service/user-service';

const routerService = RouterService.factory();
const userService = UserService.factory();

export default () => (
    <Router>
        <Layout>
            <Switch>
                <Route path={routerService.getSignInRoute()}>
                    <SignIn />
                </Route>
                <Route path={routerService.getSignUpRoute()}>
                    <SignUp />
                </Route>
                <Route path={routerService.getCartRoute()}>
                    <Cart />
                </Route>
                <PrivateRoute path={routerService.getProfileRoute()}>
                    <Profile />
                </PrivateRoute>
                <PrivateRoute path={routerService.getOrderHistoryRoute()}>
                    <OrderHistory />
                </PrivateRoute>
                <Route path={routerService.getHomeRoute()}>
                    <Home />
                </Route>
            </Switch>
        </Layout>
    </Router>
);

function PrivateRoute({ children, ...rest }) {
    return (
        <Route
            {...rest}
            render={({ location }) =>
                userService.isAuth() ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: routerService.getSignInRoute(),
                            state: { from: location }
                        }}
                    />
                )
            }
        />
    );
}